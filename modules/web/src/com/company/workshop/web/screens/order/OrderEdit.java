package com.company.workshop.web.screens.order;

import com.haulmont.cuba.gui.screen.*;
import com.company.workshop.entity.Order;

@UiController("workshop_Order.edit")
@UiDescriptor("order-edit.xml")
@EditedEntityContainer("orderDc")
@LoadDataBeforeShow
public class OrderEdit extends StandardEditor<Order> {
}
package com.company.workshop.web.screens.order;

import com.haulmont.cuba.gui.screen.*;
import com.company.workshop.entity.Order;

@UiController("workshop_Order.browse")
@UiDescriptor("order-browse.xml")
@LookupComponent("ordersTable")
@LoadDataBeforeShow
public class OrderBrowse extends StandardLookup<Order> {
}
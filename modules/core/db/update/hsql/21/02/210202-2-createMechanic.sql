alter table WORKSHOP_MECHANIC add constraint FK_WORKSHOP_MECHANIC_ON_USER foreign key (USER_ID) references SEC_USER(ID);
create index IDX_WORKSHOP_MECHANIC_ON_USER on WORKSHOP_MECHANIC (USER_ID);

package com.company.workshop.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Table(name = "WORKSHOP_MECHANIC")
@Entity(name = "workshop_Mechanic")
@NamePattern("%s|user")
public class Mechanic extends StandardEntity {
    private static final long serialVersionUID = -8403788527106191469L;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "USER_ID")
    @NotNull
    private User user;

    @NotNull
    @Column(name = "HOURLY_RATING", nullable = false)
    private BigDecimal hourlyRating;

    public BigDecimal getHourlyRating() {
        return hourlyRating;
    }

    public void setHourlyRating(BigDecimal hourlyRating) {
        this.hourlyRating = hourlyRating;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}